package ru.ncedu.springdemo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.ncedu.springdemo.services.AuthService;

@Controller
@RequestMapping("/auth")
public class AuthController {

    private final AuthService authService;

    @Autowired
    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @RequestMapping(value = "/login", method = RequestMethod.GET)
    public String authForm() {
        if (authService.isAuthorized()) {
            return "redirect:/";
        }
        return "auth";
    }

    @RequestMapping(value = "/login", method = RequestMethod.POST)
    public String authorize(Model model,
                            @RequestParam String username, @RequestParam String password) {
        boolean result = authService.authorize(username, password);
        if (result) {
            return "redirect:/";
        } else {
            model.addAttribute("error", true);
            return "auth";
        }
    }

    @RequestMapping("/logout")
    public String logout() {
        authService.logout();
        return "redirect:/";
    }
}
