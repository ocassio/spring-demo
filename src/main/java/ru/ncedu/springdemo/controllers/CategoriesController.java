package ru.ncedu.springdemo.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import ru.ncedu.springdemo.domain.Category;
import ru.ncedu.springdemo.repositories.CategoriesRepository;
import ru.ncedu.springdemo.services.AuthService;

import java.util.List;

@Controller
public class CategoriesController {

    private final CategoriesRepository categoriesRepository;
    private final AuthService authService;

    @Autowired
    public CategoriesController(CategoriesRepository categoriesRepository,
                                AuthService authService) {
        this.categoriesRepository = categoriesRepository;
        this.authService = authService;
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getIndex(Model model) {
        List<Category> categories = categoriesRepository.findAll();
        model.addAttribute("categories", categories);
        model.addAttribute("authorized", authService.isAuthorized());
        return "index";
    }

    @RequestMapping(value = "/categories/add", method = RequestMethod.GET)
    public String getAdditionForm() {
        return "redirect:/";
    }

    @RequestMapping(value = "/categories/add", method = RequestMethod.POST)
    public String add(@ModelAttribute Category category) {
        categoriesRepository.save(category);
        return "redirect:/";
    }
}
