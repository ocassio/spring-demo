package ru.ncedu.springdemo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.ncedu.springdemo.domain.Category;

import java.util.List;

public interface CategoriesRepository extends JpaRepository<Category, Long> {
    List<Category> findByNameAndDescription(String name, String description);
}
