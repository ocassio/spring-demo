package ru.ncedu.springdemo.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import ru.ncedu.springdemo.repositories.CategoriesRepository;

@RestController
@RequestMapping("/rest/categories")
public class CategoriesRestService {

    private final CategoriesRepository categoriesRepository;

    @Autowired
    public CategoriesRestService(CategoriesRepository categoriesRepository) {
        this.categoriesRepository = categoriesRepository;
    }

    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable("id") long id) {
        categoriesRepository.delete(id);
    }
}
