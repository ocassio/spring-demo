package ru.ncedu.springdemo.services;

public interface AuthService {
    boolean authorize(String username, String password);
    void logout();
    Long getCurrentUserId();
    boolean isAuthorized();
}
