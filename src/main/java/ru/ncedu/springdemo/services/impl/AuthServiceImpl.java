package ru.ncedu.springdemo.services.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.ncedu.springdemo.services.AuthService;
import ru.ncedu.springdemo.sessionbeans.AuthSessionBean;

@Service
public class AuthServiceImpl implements AuthService {

    private final AuthSessionBean authSessionBean;

    @Autowired
    public AuthServiceImpl(AuthSessionBean authSessionBean) {
        this.authSessionBean = authSessionBean;
    }

    @Override
    public boolean authorize(String username, String password) {
        // Process username and password and set user id to session on success
        long userId = (long) (Math.random() * 100);
        authSessionBean.setUserId(userId);
        return true;
    }

    @Override
    public Long getCurrentUserId() {
        return authSessionBean.getUserId();
    }

    @Override
    public boolean isAuthorized() {
        return authSessionBean.getUserId() != null;
    }

    @Override
    public void logout() {
        authSessionBean.setUserId(null);
    }
}
