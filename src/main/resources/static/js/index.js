document.querySelectorAll('.jsRemove').forEach(function (button) {
    button.onclick = function () {
        var id = button.attributes['data-id'].value;
        fetch('/rest/categories/' + id, {
            method: 'DELETE'
        }).then(function () {
            button.parentNode.parentNode.remove();
        });
    }
});