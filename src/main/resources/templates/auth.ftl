<#-- @ftlvariable name="error" type="boolean" -->
<html>
<head>
    <title>Authorization</title>
</head>
<body>
<a href="/">Main Page</a>
<h1>Authorization</h1>
<form method="post">
    <#if error!false>
        <h3>Oops, seems like you have entered wrong credentials!</h3>
    </#if>
    <input type="text" name="username" placeholder="Name"/>
    <input type="password" name="password" placeholder="Password" />
    <button type="submit">Submit</button>
</form>
</body>
</html>