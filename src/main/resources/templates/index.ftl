<#-- @ftlvariable name="authorized" type="boolean" -->
<html>
<head>
    <title>Categories</title>
</head>
<body>
    <h1>Categories</h1>
    <a href="/categories/add">Add</a>
    <#if authorized>
        <a href="/auth/logout">Log Out</a>
    <#else>
        <a href="/auth/login">Log In</a>
    </#if>
    <table>
        <tr>
            <th>ID</th>
            <th>Name</th>
            <th>Description</th>
        </tr>
        <#list categories as category>
            <tr>
                <td>${category.id}</td>
                <td>${category.name}</td>
                <td>${category.description}</td>
                <td>
                    <button data-id="${category.id}" class="jsRemove" type="button">Remove</button>
                </td>
            </tr>
        <#else>
            <tr>
                <td colspan="3">Seems like there is nothing interesting here</td>
            </tr>
        </#list>
    </table>
    <script type="text/javascript" src="js/index.js"></script>
</body>
</html>